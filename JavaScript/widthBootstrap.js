$(document).ready(function () {
    $(window).resize(function () {
        cambioW();
        topFooter();
    });
    cambioW();
    topFooter();
    function cambioW() {
        var projectsW = $(".row .col");

        if ($(window).width() < 768) {
            projectsW.removeClass();
            projectsW.addClass("col col-12");
        }
        else if ($(window).width() >= 768 && $(window).width() < 1100) {
            projectsW.removeClass();
            projectsW.addClass("col col-6");
        }
        else if ($(window).width() >= 1100 && $(window).width() < 1500) {
            projectsW.removeClass();
            projectsW.addClass("col col-4");
        }
        else {
            projectsW.removeClass();
            projectsW.addClass("col col-3");
        }

    }

    function topFooter() {
        var box = document.getElementById("box");
        alturaBox = box.clientHeight;
        var footer = document.getElementById("footer");
        footer.style.top = (alturaBox + 600) + "px";
    }

});

